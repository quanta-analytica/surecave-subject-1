To run this project, you need to follow the steps as given below:

1) Clone the repo  
2) Create a virtualenv  
3) Activate virtualenv & cd to src folder  
4) Install requirements with pip install -r requirements.txt  
5) Runserver  