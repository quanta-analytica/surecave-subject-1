from django.contrib import admin

# Register your models here.
from .models import Property,PropertyLease,UserPayment

admin.site.register(Property)
admin.site.register(PropertyLease)
admin.site.register(UserPayment)