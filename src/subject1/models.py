from django.db import models
from django.conf import settings
# Create your models here.
User = settings.AUTH_USER_MODEL

class Property(models.Model):
	owner = models.ForeignKey(User,on_delete = models.CASCADE)
	name = models.CharField(max_length = 255)
	image = models.ImageField(blank=True,null=True)
	slug = models.SlugField()
	address = models.TextField()
	tenants = models.ManyToManyField(User,related_name  = "tenants",blank=True,null=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)



	def __str__(self):
		return self.name

	def get_property_detail_url(self):

		return f"/property/{self.slug}"
	def get_add_tenant_url(self):

		return f"/property/{self.slug}/add-tenant"

	def get_add_lease_url(self):

		return f"/property/{self.slug}/add-lease"

	def get_tenant_payment_url(self):

		return f"/property/{self.slug}/payment/detail/"



class PropertyLease(models.Model):
	property_to_lease = models.ForeignKey('Property',on_delete=models.CASCADE)
	rent = models.FloatField()
	initial_security_deposit = models.FloatField()
	lease_start_date = models.DateField()
	lease_end_date = models.DateField()
	initial_amount_paid = models.BooleanField(default=False)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

	def __str__(self):
		return str(self.rent)

class UserPayment(models.Model):
	user = models.ForeignKey(User,on_delete = models.CASCADE)
	amount_paid = models.FloatField()
	charge_id = models.CharField(max_length=255)
	property_paid = models.ForeignKey('Property',on_delete=models.CASCADE)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

	def __str__(self):
		return self.user.first_name


