from django.shortcuts import render

# Create your views here.
from .models import Property,UserPayment
from .forms import EmailForm,PropertyLeaseForm
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.conf import settings
from django.db.models import Sum,Count
from django.urls import reverse
User = get_user_model()
import stripe
from django.contrib import messages

stripe.api_key = settings.STRIPE_API_KEY



def home(request):
	return HttpResponseRedirect(reverse('account_login'))



def user_profile(request):
	template = "profile.html"
	context = {}
	return render(request,template,context)


def dashboard(request):
	template = "dashboard.html"
	print(request.user.landlord_tenant)
	properties = None
	if request.user.landlord_tenant == "Landlord":
		properties = Property.objects.filter(owner= request.user)

	elif request.user.landlord_tenant == "Tenant":
		try:
			properties = Property.objects.get(tenants=request.user)
		except Property.DoesNotExist:
			properties = None
		

	context = {
		"properties":properties,
		"key" : settings.STRIPE_PUBLISHABLE_KEY,
	}

	return render(request,template,context)



def property_detail(request,slug):

	property_obj = Property.objects.get(slug = slug)
	total_amount_paid = Property.objects.aggregate(Sum('userpayment__amount_paid'))
	print(total_amount_paid)
	number_of_tenants = len(property_obj.tenants.all())
	number_of_payments_made = Property.objects.aggregate(Count('userpayment__amount_paid'))['userpayment__amount_paid__count']


	template = "property-detail.html"
	print("Inside property detail with slug being", slug)

	property_lease = property_obj.propertylease_set.all()
	yearly_earning = 0
	if property_lease:
		property_lease = property_lease[0]
		yearly_earning = property_lease.rent * 12
	amount_to_be_paid = 0.00

	if request.method == "POST":
		print("Post request initiated")

		print("Got a token back from Stripe",request.POST['stripeToken'])
		print("Got the amount",request.POST['amount'])

		charge = stripe.Charge.create(
			amount=request.POST['amount'],
			currency='usd',
			description='A Django charge',
			source=request.POST['stripeToken']
		)
		print("The charge id is",charge["id"])
		print(charge["paid"] == True)
		print(charge["status"] == "succeeded")
		print(charge["amount"])

		if charge["status"] == "succeeded":


			instance,created = UserPayment.objects.get_or_create(user=request.user,
																 amount_paid = charge["amount"],
																 charge_id =charge["id"],
																 property_paid = property_obj)
			if created:
				print("created a new instance")
			else:
				print("did not creat a new one.. something can be wrong thoug..")



	context = {
		"property_obj":property_obj,
		"key" : settings.STRIPE_PUBLISHABLE_KEY,
		"property_lease":property_lease,
		"total_amount_paid":total_amount_paid['userpayment__amount_paid__sum'],
		"yearly_earning":yearly_earning,
		"number_of_tenants":number_of_tenants,
		"number_of_payments_made":number_of_payments_made,
	}

	return render(request,template,context)



def add_tenant(request,slug):
	template = "add_tenant.html"
	property_obj = Property.objects.get(slug=slug)
	form = EmailForm(request.POST or None)
	if form.is_valid():
		email = form.cleaned_data.get("email")
		try:
			user = User.objects.get(email=email)
			property_obj.tenants.add(user)
			property_obj.save()
			print("Form saved")
			messages.success(request, 'Tenant Added Successfully')
			return HttpResponseRedirect(reverse("dashboard"))
		except User.DoesNotExist:
			messages.error(request, 'Tenant Not Found. Try sending an invite again.')

	context = {
		"form":form,
	}
	return render(request,template,context)


def add_property_lease(request,slug):
	template = "add_lease.html"
	property_obj = Property.objects.get(slug=slug)
	form = PropertyLeaseForm(request.POST or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.property_to_lease = property_obj
		instance.save()

		print("--- Lease form saved ---")
		messages.success(request,"Lease was setup successfully")
		return HttpResponseRedirect(reverse("property-detail",kwargs={"slug":property_obj.slug}))


	context = {
		"form":form,
	}

	return render(request,template,context)


def tenant_payment_detail(request,slug):
	template = "tenant_payment_detail.html"
	property_obj = UserPayment.objects.filter(property_paid__slug=slug)
	context = {
		"property_obj" : property_obj,
	}
	return render(request,template,context)





