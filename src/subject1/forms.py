from django import forms
from .models import PropertyLease
from django.forms import DateInput




class EmailForm(forms.Form):
	email = forms.EmailField()



class PropertyLeaseForm(forms.ModelForm):
	   # teacher_student = forms.CharField(max_length=10,label='phone_number')
	class Meta:
		model = PropertyLease
		fields = ['rent','initial_security_deposit','lease_start_date','lease_end_date']
		widgets = {
			'lease_start_date': DateInput(attrs={'type': 'date'}),
			'lease_end_date': DateInput(attrs={'type': 'date'}),
		}