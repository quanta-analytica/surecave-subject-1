"""surecave URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include, re_path
from subject1.views import home,property_detail,add_tenant,add_property_lease,dashboard,user_profile,tenant_payment_detail
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', home, name='home'),
    re_path(r'^accounts/', include('allauth.urls')),
    path('accounts/profile/', user_profile, name='account_profile'),
    path('dashboard/',dashboard,name="dashboard"),
    path('property/<str:slug>/', property_detail,name="property-detail"),
    path('property/<str:slug>/add-tenant/', add_tenant,name="add-tenant"),
    path('property/<str:slug>/add-lease/', add_property_lease,name="add-lease"),
    path('property/<str:slug>/payment/detail/', tenant_payment_detail,name="tenant-payment-detail"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
