from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser,BaseUserManager
# Create your models here.
User = settings.AUTH_USER_MODEL
from datetime import datetime





class User(AbstractUser):
	PERSON_TYPE_CHOICES = (
	    ('Landlord', ('Landlord')),
	    ('Tenant', ('Tenant ')),
	)
	landlord_tenant = models.CharField(max_length=10,choices = PERSON_TYPE_CHOICES)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	middle_name = models.CharField(max_length=50, blank=True,null=True)
	phone_number = models.IntegerField(blank=True,null=True)
	date_of_birth = models.DateField(blank=True,null=True)


	def __unicode__(self):
	    return self.teacher_student