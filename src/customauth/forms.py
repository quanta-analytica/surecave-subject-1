from .models import *
from django import forms

class SignupForm(forms.ModelForm):
	   # teacher_student = forms.CharField(max_length=10,label='phone_number')
	class Meta:
		model = User
		fields = ['email','landlord_tenant','first_name','last_name','middle_name','phone_number','date_of_birth']


 

	def signup(self, request, user):

		user.landlord_tenant = self.cleaned_data['landlord_tenant']
		user.save()