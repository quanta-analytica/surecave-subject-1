# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth import get_user_model
# from .forms import UserAdminCreationForm, UserAdminChangeForm
# Register your models here.
from .models import User

User = get_user_model()


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email','first_name','last_name')
    
    fieldsets = (
        (None, {'fields': ('email', 'password','first_name','last_name','middle_name','phone_number','date_of_birth','landlord_tenant','username','is_active','is_staff')}),
       # ('Full name', {'fields': ()}),
       
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email','first_name','username')
    ordering = ('email',)
    filter_horizontal = ()
admin.site.register(User, UserAdmin)